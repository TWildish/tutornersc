## This tutorial shows how exercise basic operations on Cori using Shifter and CERN binaries from PDSF

### Prerequisits
* NERSC credentials
* authorized to login to Cori/Edison
* 'repo' with non-zero allocation

### Step 0 : login to Cori, 

[laptop]
ssh -X cori.nersc.gov

### step 1 : explore Cori login node
[balewski@cori07 login node]
ls      # do you see your PDSF home dir?
ls >aa  # can you write to your home dir?
ls -l  /project/projectdirs/lz  # do  you see LZ-data ?
echo $CSCRATCH
ls -l $CSCRATCH
getnim -U $USER   # can you run jobs? on what repo?


### step 2 : launch interactive session on Cori, use shared queue, request 4 vCore machine. Take advantage of reservation. Count vCores, check $CSCRATCH, check OS-version, check ~/.ssh/ access

[balewski@cori07 login node]
salloc --ntasks 4 --partition shared --time 20:00 --constraint haswell --reservation janshare --account lz
# it may take 1 minute even w/ reservation

[balewski@nid02302  interactive session]
cat /etc/*release     # verify OS=SUSE:12 is NOT the OS you want for LZ
lstopo -c | grep PU | nl    # verify you got 4 vCores
 free -g     # check avaliable RAM and swap space

### step 3 :  find Shifter image with SL6.4, instantiate it, verify OS, $SCRATCH and ~/ access

[balewski@nid02302]
shifterimg images | grep chos    # do you see 'chos-sl64:v1' ?
shifter --image=custom:pdsf-chos-sl64:v1 bash   # instantiate this image

[bash-4.1 inside your OS]
cat /etc/*release               # do you see OS=SL6.4 ?
env|grep  SHIFTER_RUNTIME       # returns 1 if you are inside shifter image
ls -l ~/
ls -l $CSCRATCH
ls -l ~/.ssh/

### step 4 : load common PDSF software  (ROOT,CLHEP,Geant4), launch root

source ~/tutorNersc/2017_lz/myLzSetup.source
root -b -q
root -l
 new TCanvas
.q


### step 5 :  cleanup and exit 3x
bash-4.1$ exit # from shifter

balewski@nid02086:~> exit  # from  interactive node
#  salloc: Relinquishing job allocation 3831915

balewski@cori07:~> exit  # from cori
#  Connection to cori.nersc.gov closed.


