#!/bin/bash
echo create large files with different md5-sum signature
 
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

nMB=1000

for i in {1..4}; do
 coreN=big${nMB}MB.${i}.dat
 dd if=/dev/zero of=$coreN bs=1M count=$nMB
 echo $RANDOM >> $coreN
 chmod 0446 $coreN
 echo done $coreN
 #md5sum  $coreN
done
 
ls -lh big*dat
